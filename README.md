## Prerequisites:
   1. If you don't already have, install virtualenv: `pip install virtualenv`
   2. Create a virtual environment `virtualenv venv --distribute`
   3. Activate the environment `source venv/bin/activate`
   4. Install required packages `pip install -r requirements.txt`

## Using The API:
Once you have finished with the Prerequisites start up the server:
```sh
python api.py
```
To access any of the endpoints you will need to login first to request a token:
```python
from requests import get, post, put, delete

data = {
    'username': 'test',
    'password': 'test'
}
response = post('http://localhost:5000/login', json=data).json()
token = 'Bearer {}'.format(response['access_token'])
```

To log out and invalidate the token just call:
```python
delete('http://localhost:5000/logout', headers={'Authorization': token}).json()
```

To obtain a list of tasks:
```python
get('http://localhost:5000/todo/api/v1.0/tasks', headers={'Authorization': token}).json()
```

To obtain details for a specific task:
```python
get('http://localhost:5000/todo/api/v1.0/tasks/1', headers={'Authorization': token}).json()
```

To obtain all tasks for given user (append the user ID to the end of `.../tasks/user/`):
```python
get('http://localhost:5000/todo/api/v1.0/tasks/user/2', headers={'Authorization': token}).json()
```

To create a task:
```python
post('http://localhost:5000/todo/api/v1.0/tasks', json={'title': 'Read a book', 'user': 1}, headers={'Authorization': token}).json()
```

To update a task:
```python
put('http://localhost:5000/todo/api/v1.0/tasks/3', json={'done': True}, headers={'Authorization': token}).json()
```

To delete a task:
```python
delete('http://localhost:5000/todo/api/v1.0/tasks/3', headers={'Authorization': token}).json()
```

## Running The Tests
Make sure you have activated the virtual environment then run `python test_api.py`

###### This API was created using Python 3.6.0.


