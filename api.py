from flask import Flask, jsonify, make_response, request
from flask_restful import Api, abort
from auth import authenticate
from tasks import tasks

from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_raw_jwt
)

app = Flask(__name__)
api = Api(app)
TASKS_ENDPOINT = '/todo/api/v1.0/tasks'

app.config['SECRET_KEY'] = 'super-secret'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)

blacklist = set()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return jti in blacklist


@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    user = authenticate(username, password)
    if not user:
        return jsonify({"msg": "Bad username or password"}), 401

    # Identity can be any data that is json serializable
    access_token = create_access_token(identity=user.id)
    return jsonify(access_token=access_token), 200


@app.route('/logout', methods=['DELETE'])
@jwt_required
def logout():
    """Endpoint for revoking the current users access token"""
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('{}'.format(TASKS_ENDPOINT), methods=['GET'])
@jwt_required
def get_tasks():
    """
    GET ALL TASKS
    """
    return jsonify({'tasks': tasks})


@app.route('{}/<int:task_id>'.format(TASKS_ENDPOINT), methods=['GET'])
@jwt_required
def get_task(task_id):
    """
    GET INDIVIDUAL TASK
    :param task_id: the ID of task to retrieve
    """
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    return jsonify({'task': task[0]})


@app.route('{}/user/<int:user_id>'.format(TASKS_ENDPOINT), methods=['GET'])
@jwt_required
def get_user_tasks(user_id):
    user_tasks = [task for task in tasks if task['user'] == user_id]
    if len(user_tasks) == 0:
        abort(404)
    return jsonify({'count': len(user_tasks), 'tasks': user_tasks})


@app.route('{}'.format(TASKS_ENDPOINT), methods=['POST'])
@jwt_required
def create_task():
    """
    CREATE TASK ENDPOINT
      - must be json and a title is required
    """
    if not request.json or not 'title' in request.json:
        abort(400)
    task = {
        'id': tasks[-1]['id'] + 1,
        'user': request.json.get('user', ''),
        'title': request.json['title'],
        'description': request.json.get('description', ''),
        'done': False
    }
    tasks.append(task)
    return jsonify({'task': task}), 201


@app.route('{}/<int:task_id>'.format(TASKS_ENDPOINT), methods=['PUT'])
@jwt_required
def update_task(task_id):
    """
    UPDATE TASK ENDPOINT
    :param task_id: the ID of the task to be updated
     - if updating the title it must be unicode
    """
    task = [task for task in tasks if task['id'] == task_id]
    if len(tasks) == 0:
        abort(404)
    if not request.json:
        abort(400)

    if 'title' in request.json:
        if type(request.json['title']) is not str:
            abort(400)
        task[0]['title'] = request.json['title']

    if 'description' in request.json:
        if type(request.json['description']) is not str:
            abort(400)
        task[0]['description'] = request.json['description']

    if 'done' in request.json:
        if type(request.json['done']) is not bool:
            abort(400)
        task[0]['done'] = request.json['done']

    return jsonify({'task': task[0]})


@app.route('{}/<int:task_id>'.format(TASKS_ENDPOINT), methods=['DELETE'])
@jwt_required
def delete_task(task_id):
    """
    DELETE TASK ENDPOINT
    :param task_id: the ID of the task to be deleted
    """
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    tasks.remove(task[0])
    return jsonify({'result': True})


if __name__ == '__main__':
    app.run(debug=True)
