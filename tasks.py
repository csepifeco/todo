tasks = [
    {
        'id': 1,
        'user': 1,
        'title': u'Buy groceries',
        'description': u'Oreo, Milk, Orange, Cheese',
        'done': False
    },
    {
        'id': 2,
        'user': 1,
        'title': u'Make cake',
        'description': u'Need to make that nice cheesecake',
        'done': False
    },
    {
        'id': 3,
        'user': 2,
        'title': u'Make milkshake',
        'description': u'Oreo milkshake is the best',
        'done': False
    }
]