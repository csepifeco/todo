import json
import unittest
from api import app, TASKS_ENDPOINT
from auth import authenticate, identity
from flask_jwt import JWT
jwt = JWT(app, authenticate, identity)


class ApiTestCase(unittest.TestCase):
    """
    Test cases for Todo CRUD
    """

    def setUp(self):
        self.app = app.test_client()
        payload = {
            'username': 'test',
            'password': 'test'
        }
        res = self.app.post('/login',
                            data=json.dumps(payload),
                            content_type='application/json')
        self.token = 'Bearer {}'.format(json.loads(res.data)['access_token'])
        self.headers = {
            'Authorization': self.token
        }

    def tearDown(self):
        pass

    def test_login(self):
        # Successful login
        p1 = {
            'username': 'test',
            'password': 'test'
        }
        res = self.app.post('/login', data=json.dumps(p1),
                            content_type='application/json')
        self.assertEqual(res.status_code, 200)
        self.assertIsNotNone(json.loads(res.data)['access_token'])

        # Unsuccessful login - bad password
        p2 = {
            'username': 'test',
            'password': 'badpassword'
        }
        res2 = self.app.post('/login', data=json.dumps(p2),
                            content_type='application/json')
        self.assertEqual(res2.status_code, 401)

        # Unsuccessful login - missing details
        res3 = self.app.post('/login', data=json.dumps({}),
                            content_type='application/json')
        self.assertEqual(res3.status_code, 400)

    def test_logout(self):
        res = self.app.delete('/logout')
        self.assertEqual(res.status_code, 401)
        res2 = self.app.delete('/logout', headers={'Authorization': self.token})
        self.assertEqual(res2.status_code, 200)
        # After successful logout users should not be able to retrieve
        # information using the original token
        res3 = self.app.get(TASKS_ENDPOINT, headers=self.headers)
        self.assertEqual(res3.status_code, 401)

    def test_headers(self):
        # Bad token
        res = self.app.get(TASKS_ENDPOINT,
                           headers={'Authorization': 'Bearer badtoken'})
        self.assertEqual(res.status_code, 422)

        # Missing header
        res2 = self.app.get(TASKS_ENDPOINT)
        self.assertEqual(res2.status_code, 401)

    def test_get_all_tasks(self):
        # Get all tasks
        res = self.app.get(TASKS_ENDPOINT, headers=self.headers)
        self.assertEqual(res.status_code, 200)

    def test_get_task_details(self):
        # Create a task
        task1 = self.app.post(TASKS_ENDPOINT,
                            data=json.dumps({'title': 'Wibble'}),
                            headers=self.headers,
                            content_type='application/json')
        new_task_id = json.loads(task1.data)['task']['id']

        # Get details of a specific task (by ID)
        res = self.app.get('{}/{}'.format(TASKS_ENDPOINT, new_task_id),
                            headers=self.headers)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.data)['task']['id'], new_task_id)

    def test_get_tasks_for_user(self):
        user_id = 5
        # Create a task
        task1 = self.app.post(TASKS_ENDPOINT,
                              data=json.dumps(
                                  {'title': 'Wibble', 'user': user_id}),
                              headers=self.headers,
                              content_type='application/json')
        task2 = self.app.post(TASKS_ENDPOINT,
                              data=json.dumps(
                                  {'title': 'Wobble', 'user': user_id}),
                              headers=self.headers,
                              content_type='application/json')
        # Get a list of tasks for given user (by ID)
        res = self.app.get('{}/user/{}'.format(TASKS_ENDPOINT, user_id),
                           headers=self.headers)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.data)['count'], 2)

    def test_create_task(self):
        # Successful if title is provided
        payload = {
            'title': 'Wibble wobble',
            'user': 1
        }
        res = self.app.post(TASKS_ENDPOINT, data=json.dumps(payload),
                            headers=self.headers,
                            content_type='application/json')
        self.assertEqual(res.status_code, 201)
        results = json.loads(res.data)
        self.assertEqual(results['task']['title'], payload['title'])
        self.assertEqual(results['task']['user'], payload['user'])

        # Should fail if title is not provided
        res2 = self.app.post(TASKS_ENDPOINT,
                             data=json.dumps({'name': 'Wobble'}),
                             headers=self.headers,
                             content_type='application/json')
        self.assertEqual(res2.status_code, 400)

    def test_update_task(self):
        # Create a task
        res = self.app.post(TASKS_ENDPOINT,
                            data=json.dumps({'title': 'Wibble'}),
                            headers=self.headers,
                            content_type='application/json')
        new_task_id = json.loads(res.data)['task']['id']

        # Then mark it as done
        payload = {
            'done': True
        }
        res2 = self.app.put('{}/{}'.format(TASKS_ENDPOINT, new_task_id),
                            data=json.dumps(payload),
                            headers=self.headers,
                            content_type='application/json')
        self.assertEqual(res2.status_code, 200)
        self.assertTrue(json.loads(res2.data)['task']['done'])

    def test_delete_task(self):
        # Create a task
        res = self.app.post(TASKS_ENDPOINT,
                            data=json.dumps({'title': 'Delete me!'}),
                            headers=self.headers,
                            content_type='application/json')
        new_task_id = json.loads(res.data)['task']['id']

        # Then delete it
        res2 = self.app.delete('{}/{}'.format(TASKS_ENDPOINT, new_task_id),
                           headers=self.headers)
        self.assertEqual(res2.status_code, 200)

if __name__ == '__main__':
    unittest.main()